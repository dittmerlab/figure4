# ----------------------------------------------------------------------------
### Load libraries
#
library(ggplot2)  # For ggplot()
library(reshape2)  # For melt()

# ----------------------------------------------------------------------------
### User-defined functions
#
PlotBoxCox <- function(data.plot, cell.line.name) {
  # Make Box-Cox plots for different clones
  #
  # Args:
  #   data.plot: Data to be plotted.
  #   cell.line.name: Name of cell line.
  #
  # Returns:
  #   A ggplot object.
  a <- ggplot(aes(Clone, Response), data = data.plot)
  a <- a + geom_boxplot(fill = "lightgray", col = "black")
  a <- a + geom_point(size = I(5), alpha = I(0.8),
                      aes(color = Clone, shape = PlateNo),
                      position = position_jitter(w = 0.2, h = 0.0))
  a <- a + scale_color_brewer(palette = "Set1")
  a <- a + theme_bw()
  a <- a + labs(title = cell.line.name) + xlab("Hours") + ylab("Response")
    
  # Set parameters for the grids
  a <- a + facet_grid( .~ Hour)
  a <- a + theme(strip.text.x = element_text(size = 16))
  a <- a + theme(panel.grid.minor = element_blank())
  a <- a + theme(panel.grid.major = element_blank())
    
  # Set parameters for the legend
  a <- a + theme(legend.position = "right")
  a <- a + theme(legend.title = element_text(size = 14))
  a <- a + theme(legend.text = element_text(size = 12))
  a <- a + theme(legend.key = element_rect(colour = "white"))

  # Set parameters for the titles and text
  a <- a + theme(plot.title = element_text(size = 20, margin = margin(0, 0, 10, 0)))
  a <- a + theme(axis.title.x = element_text(size = 16, margin = margin(15, 0, 0, 0)))
  a <- a + theme(axis.title.y = element_text(size = 16, margin = margin(0, 15, 0, 0)))
  a <- a + theme(axis.text.x = element_text(size = 16, angle = 90))
  a <- a + theme(axis.text.y = element_text(size = 16))
    
  plot(a)
}

# ----------------------------------------------------------------------------
### Load data
#
data.orgn.1 <- read.csv("toR_nucleofection_JSC1_BC1_BJAB.csv")
data.orgn.2 <- read.csv("toR_nucleofection_Namalwa_BC3.csv")

data.whole.1 <- melt(data = data.orgn.1, id.vars = c("Hour", "PlateNo"))
data.whole.2 <- melt(data = data.orgn.2, id.vars = c("Hour", "PlateNo"))

data.whole <- rbind(data.whole.1, data.whole.2)

colnames(data.whole) <- c("Hour", "PlateNo", "Variable", "Response")

levels.cell.lines <- c("JSC1", "BC1", "BJAB", "NAM", "BC3")  # Kinds of cell lines
data.whole$CellLine <- data.whole$Clone <- NA

for(cl in 1:length(levels.cell.lines)) {
  
  cell.line <- levels.cell.lines[cl]

  variable <- data.whole$Variable
  
  idx.cell.line <- substr(variable, start = 1, stop = nchar(cell.line)) == cell.line
  
  data.whole$CellLine[idx.cell.line] <- cell.line
  data.whole$Clone[idx.cell.line] <- gsub(cell.line, "", data.whole$Variable[idx.cell.line])
}

# Exclude Media samples from analysis
data.whole <- subset(data.whole,
                     select = -Variable,
                     subset = substr(Variable, start = 1, stop = 5) != "Media")

### Change the level names of factors
data.whole$CellLine <- as.factor(data.whole$CellLine)
data.whole$Clone <- as.factor(data.whole$Clone)

## Change the names of cell lines and clones for plotting
levels(data.whole$CellLine) <- c("BC1", "BC3", "BJAB", "JSC1", "Namalwa")
levels(data.whole$Clone) <- c("Parent", "sg126", "sg45", "GFP", "Mock")

data.whole$PlateNo <- as.factor(data.whole$PlateNo)

# Remove 'h' from the variable 'Hour' to obtain hours in numbers
data.whole$HourNum <- as.numeric(gsub("h", "", data.whole$Hour))

# ----------------------------------------------------------------------------
### Log-transform the response values
#
data.log <- data.whole
data.log$Response <- log(data.whole$Response)

# Kinds of cell lines
levels.cell.lines <- levels(data.log$CellLine)

# ----------------------------------------------------------------------------
### Figure 4D
#
cell.line <- "BC3"
output.file.name <- "figure_4D.png"

# Exclude the parent cells from the analysis
data.cell.line <- subset(data.log, 
                         subset = (CellLine == cell.line)&(Clone != "Parent"))
data.cell.line <- droplevels(data.cell.line)
data.cell.line$Clone <- factor(data.cell.line$Clone,
                               levels = c("Mock", "GFP", "sg126", "sg45"))

png(filename = output.file.name,
    width = 8, height = 5, res = 300, units = "in")

PlotBoxCox(data.cell.line, cell.line)

dev.off()

# ----------------------------------------------------------------------------
### Figure 4E
#
cell.line <- "Namalwa"  
output.file.name <- "figure_4E.png"

# Exclude the parent cells from the analysis
data.cell.line <- subset(data.log, 
                         subset = (CellLine == cell.line)&(Clone != "Parent"))
data.cell.line <- droplevels(data.cell.line)
data.cell.line$Clone <- factor(data.cell.line$Clone,
                               levels = c("Mock", "GFP", "sg126", "sg45"))

png(filename = output.file.name,
    width = 8, height = 5, res = 300, units = "in")

PlotBoxCox(data.cell.line, cell.line)

dev.off()

# ----------------------------------------------------------------------------
### Figure 4F
#
cell.line <- "JSC1"
output.file.name <- "figure_4F.png"

# Exclude the parent cells from the analysis
data.cell.line <- subset(data.log, 
                         subset = (CellLine == cell.line)&(Clone != "Parent"))
data.cell.line <- droplevels(data.cell.line)
data.cell.line$Clone <- factor(data.cell.line$Clone,
                               levels = c("Mock", "GFP", "sg126", "sg45"))

png(filename = output.file.name,
    width = 8, height = 5, res = 300, units = "in")

PlotBoxCox(data.cell.line, cell.line)

dev.off()

# ----------------------------------------------------------------------------
### Figure 4G
#
cell.line <- "BC1"
output.file.name <- "figure_4G.png"

# Exclude the parent cells from the analysis
data.cell.line <- subset(data.log, 
                         subset = (CellLine == cell.line)&(Clone != "Parent"))
data.cell.line <- droplevels(data.cell.line)
data.cell.line$Clone <- factor(data.cell.line$Clone,
                               levels = c("Mock", "GFP", "sg126", "sg45"))

png(filename = output.file.name,
    width = 8, height = 5, res = 300, units = "in")

PlotBoxCox(data.cell.line, cell.line)

dev.off()